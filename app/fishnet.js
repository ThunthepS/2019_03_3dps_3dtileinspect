// Input
var inputFishnetFile = './Data/FishnetAugsburgFME_Final.json';
var inputFishnetPointFile = './Data/FishnetAugsburgFME_point_buffer.json';
var inputHeatmap = './Data/AccidentAugburgSHP_Density.geojson';
var fishnetPolygon, heatmapPolygon;
var geojsonOptions = {
    clampToGround : true
};
// Load neighborhood boundaries from KML file
var fishnetPolygonPromise = Cesium.GeoJsonDataSource.load(inputFishnetFile, geojsonOptions);
fishnetPolygonPromise.then(function(dataSource) {
    viewer.dataSources.add(dataSource);
    fishnetPolygon = dataSource.entities;
    fishnetPolygon.show = false;
    var neighborhoodEntities = dataSource.entities.values;
    for (var i = 0; i < neighborhoodEntities.length; i++) {
        var entity = neighborhoodEntities[i];
        if (Cesium.defined(entity.polygon)) {
            var AccidentRate = entity.properties.SummarizeValue._value;
            if (AccidentRate <= 0.05) {
                entity.polygon.material = new Cesium.Color(1, 0.9, 0.9, 0.1);
            } else if (AccidentRate <= 0.10) {
                entity.polygon.material = new Cesium.Color(1, 0.9, 0.9, 0.2);
            } else if (AccidentRate <= 0.16) {
                entity.polygon.material = new Cesium.Color(1, 0.75, 0.75, 0.25);
            } else if (AccidentRate <= 0.22) {
                entity.polygon.material = new Cesium.Color(1, 0.50, 0.50, 0.5);
            } else if (AccidentRate <= 0.30) {
                entity.polygon.material = new Cesium.Color(1, 0.25, 0.25, 0.8);
            } else if (AccidentRate <= 1) {
                entity.polygon.material = new Cesium.Color(1, 0, 0, 0.9);
            }
            entity.polygon.outline = false;
            // entity.polygon.outlineColor = Cesium.Color(1, 1, 1, 0);
            entity.polygon.extrudedHeight = entity.properties.SummarizeValue._value * 100; 
        }
    }
});
var heatmapPromise = Cesium.GeoJsonDataSource.load(inputHeatmap, geojsonOptions);
heatmapPromise.then(function(dataSource) {
    viewer.dataSources.add(dataSource);
    heatmapPolygon = dataSource.entities;
    heatmapPolygon.show = false;
    var neighborhoodEntities = dataSource.entities.values;
    for (var i = 0; i < neighborhoodEntities.length; i++) {
        var entity = neighborhoodEntities[i];
        if (Cesium.defined(entity.polygon)) {
            var heatmapClass = entity.properties.class._value;
            if (heatmapClass >= 7) {
                entity.polygon.material = new Cesium.Color(1, 0, 0, 0.75);
            } else if (heatmapClass == 6) {
                entity.polygon.material = new Cesium.Color(1, 0.10, 0.10, 0.75);
            } else if (heatmapClass == 5) {
                entity.polygon.material = new Cesium.Color(1, 0.25, 0.25, 0.6);
            } else if (heatmapClass == 4) {
                entity.polygon.material = new Cesium.Color(1, 0.25, 0.25, 0.5);
            } else if (heatmapClass == 3) {
                entity.polygon.material = new Cesium.Color(1, 0.5, 0.5, 0.5);
            } else if (heatmapClass == 2) {
                entity.polygon.material = new Cesium.Color(1, 0.75, 0.75, 0.5);
            } else if (heatmapClass == 1) {
                entity.polygon.material = new Cesium.Color(1, 1, 1, 0.25);
            } else if (heatmapClass == 0) {
                entity.polygon.material = new Cesium.Color(1, 1, 1, 0);
            } 
            entity.polygon.outline = false;
            // entity.polygon.outlineColor = Cesium.Color(1, 1, 1, 0);
        }
    }
});
// var fishnetPointPromise = Cesium.GeoJsonDataSource.load(inputFishnetPointFile, geojsonOptions);
// var fishnetPoint;
// fishnetPointPromise.then(function(dataSource) {
//     viewer.dataSources.add(dataSource);
//     fishnetPoint = dataSource.entities;
//     fishnetPolygon.show = false;
//     var neighborhoodEntities = dataSource.entities.values;
//     for (var i = 0; i < neighborhoodEntities.length; i++) {
//         var entity = neighborhoodEntities[i];

//         if (Cesium.defined(entity.polygon)) {
//             entity.polygon.material = Cesium.Color.fromRandom({
//                 minimumRed : 0.3,
//                 maximumGreen : 0.2,
//                 maximumBlue : 0.2,
//                 alpha : 0.25
//             });
//             entity.polygon.extrudedHeight = Math.floor(Math.random() * 100); 
//         }
//     }
// });