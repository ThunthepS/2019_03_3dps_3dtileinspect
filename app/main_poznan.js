Cesium.BingMapsApi.defaultKey = 'ApOW9LMkerqWIVSnFauilSeaZyp8df66byy1USCTjgTdMvhb4y1iAhEsUHQfCgzq'
Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmOGQ1ODcwNC0zNDU1LTQ5ZGEtOWE4YS0zYzNmYTcxOWQxYTUiLCJpZCI6MTAxMywiaWF0IjoxNTI2NjM1Nzg4fQ.X43-3OHqmcwPP5qCE2NqSGJUsj5FGWgXAzitvrV0pwY';

var viewer = new Cesium.Viewer('cesiumContainer', {
    terrainProvider: Cesium.createWorldTerrain(),
    baseLayerPicker: true,
    homeButton: true,
    scene3DOnly: true,
    //terrainExaggeration: 1.5,
    navigationHelpButton: false,
    selectionIndicator: false,
    sceneModePicker: false,
    geocoder: true,
    shadows: false,
});
// Basic Set Up for the Cesium Globe
viewer.scene.globe.enableLighting = true;
viewer.clock.shouldAnimate = true; // default00
viewer.clock.startTime = Cesium.JulianDate.fromIso8601("2018-11-24T12:00:00Z");
viewer.clock.stopTime = Cesium.JulianDate.fromIso8601("2018-11-24T18:00:00Z");
viewer.clock.currentTime = Cesium.JulianDate.fromIso8601("2018-11-24T12:00:00Z");
viewer.clock.multiplier = 0.5; // sets a speedup
viewer.clock.clockRange = Cesium.ClockRange.LOOP_STOP; // loop at the end
viewer.timeline.zoomTo(viewer.clock.startTime, viewer.clock.stopTime); // set visible range

var tileset, tileset_door, tileset_roof, tileset_ground;
var tileset_wuestenrot;
var heightOffset = 49;

//Tileset Stoeckach
var tileset;
var addLayers_3D_poznan = function (request_url) {
    // CesiumStyleChoice.style.display = "block";
    // $("#CesiumStyleChoice").show("1000");
    try {
        tileset = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: "./Datas_3D/poznan/tileset.json",
            // url: "./Datas_3D/test-stoeckach-roof-noagg-overideParrent/tileset.json",
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    tileset.readyPromise.then(function () {
        var boundingSphere = tileset.boundingSphere;
        var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
        var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
        var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
        var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
        tileset.modelMatrix = Cesium.Matrix4.fromTranslation(translation);

        viewer.zoomTo(tileset);
        $("#setPVstyle").show(500);
        $("#setNormalstyle").show(500);
    });
    // Styling the Defalut style
    // var transparentStyle = new Cesium.Cesium3DTileStyle({
    //     color: "color('WHITE', 1)",
    //     show: true
    // });
    // tileset_SK.style = transparentStyle;
};


var normalStyle = new Cesium.Cesium3DTileStyle({
    color: "color('WHITE', 1)",
    show: true
});

var setNormalStyle = function () {
    tileset.style = normalStyle;
}

var heatDemandStyle = new Cesium.Cesium3DTileStyle({
    color: {
                conditions: [
                    ["${file_candidate_gmlid} === 'BID_225920_5c55df01-dae5-4848-9de1-334401fcba15'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_114983_bcee3ddb-0866-4baa-a954-51c000583844'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_205587_2b9e490f-748b-4857-a33e-f09f571a3d72'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_206105_0a2c9794-08a6-4487-b94a-6d7e5b50a0d4'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_41126_a990c087-16a9-4a8c-b763-b1866e6342ae'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_40822_2695ce48-9f17-4dcd-ae8e-b489626889b6'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_178030_0ee62b25-588f-4722-9182-77377391c37b'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_114412_71da4146-6df9-48c1-9f6f-174743e075b8'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115034_83574422-5840-4a9a-b12b-62bda7cc3484'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_207726_5f149bc7-9ea9-4677-a4af-ba985121c69c'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_226940_5b59399c-8618-4e87-b3d6-58bdba8ccfe7'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_213403_0a23ddac-4bd8-4a75-b705-5bb4b9b0ad01'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_219043_cb28d87c-494d-42e3-81ec-adb68c9dc206'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_41124_fc7a493e-812a-4949-ab3f-c55b151172c7'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_216836_0ed09ea1-f94c-4561-9cc7-b26a5333225c'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_212770_bf6bbd85-8e99-4b69-8e00-d374a104e4a9'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_176802_0194a1f4-2e2c-49cb-a339-76a4990a1422'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_43561_9c4b3010-a61a-448c-bad3-f02659c4deac'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115440_2ca94af2-375a-4b40-9324-1fbccc4607d2'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_41115_16050401-b463-4ffa-b3b4-93343d2940d7'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_225799_c0798430-9c96-4dea-9e11-053487d61571'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115103_84b00d35-7d97-4dce-89ef-701bc12f0903'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_225656_ccd1b298-952d-48c0-96e2-ebec9a322384'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115257_12e407a5-785d-48ab-89ca-425c1df3f0c3'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_114410_f31f67cd-b2fb-4e51-b590-099c5476ec66'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_98379_1e6aea3a-b3e1-4620-ac98-62fafdc2900a'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_210211_d6229093-b5d8-4eff-a19f-56fec1fa2e03'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115224_852a7c19-8c55-4d87-8a26-088c2d4968d7'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_113559_f73a7d05-34a1-4309-b420-0edfe8e7dc73'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115271_a47e1c7f-80d1-4367-8c1f-34879ac4bdfa'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115050_173c7168-1f66-4544-b095-a2b81bc15446'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115091_319ed3d3-752a-427f-a5a3-6930a7bca7f8'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_41100_be79b1db-7b98-410b-aaf3-10a490648075'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_98380_03a6a1b3-34cc-459a-98fc-1c3053f74a4c'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_198270_2d571e9a-7e84-4d84-a9d8-481356ada770'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_176775_9d095d25-5908-4c8f-b2a8-c26fd21d1555'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_43816_d72a67dd-afa7-423c-9671-5a4b33142dd2'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_41224_26e76640-4d8f-4885-a5ee-191662ee71e7'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_176803_1d05854f-33bc-4283-b4cc-92722b986c3d'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_41962_76734376-1368-4670-8818-785ead40712e'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_195731_f4a09b34-b481-4b80-882b-a5b602948ae5'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_98381_ebf77028-2d6e-4df5-a6be-0e151d08ad0b'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_113556_d3a13447-7b32-4080-a52d-ef5a033011e1'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_42949_12fe692e-bef6-4190-832a-757e608b2fdb'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_171099_ea4de046-1a60-4399-b943-7c3cdaac117a'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115246_ce55a228-9d9e-498f-9c1d-c2e66c311b0a'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_198259_79120350-8215-4021-b2d5-5c6cd7f51850'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_41060_5e115032-1cf0-4246-a665-b4a06bb5898f'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_198268_82de884b-8fbd-4787-8be1-357f7a1b385b'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_87613_da229653-5eeb-4405-8547-99948bebd17f'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115200_61f382c5-3cfe-458a-a08e-16479bb6bd27'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115016_130618b9-30dc-4a6f-ac87-f3305c6bc262'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115092_581d5547-d473-465b-b6b4-eeb49f11cef5'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_1445301_c25fb040-a133-4e98-983b-798f9f2c4ca9'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_41971_3bb6e983-bbcf-4879-ba99-5a44354bc4e4'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_43825_a9b0cea5-f2d6-4aec-84ed-b6141e112bc0'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115273_00626ca4-176c-4f8c-8016-c829aa464630'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_40674_3e4605d9-3c01-4d1c-bc82-0f2b3ac3dcac'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_199516_e69299b1-c990-4570-8d5e-625dc9693b8b'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_114928_f49389d1-ebb4-457a-a4b7-1da06e0697a3'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115199_44da8032-4cb6-4808-a425-34ce668e5fcb'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_176579_a7645992-f98d-4dc4-838f-a92275f050a7'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_198213_5f909032-a259-45a5-9f3c-3e62bff1d0d3'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115244_e8fe77f5-ff28-47f3-ad2f-b6442b3bd5f1'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115213_e9229552-9ce5-4dfe-8db8-f59e66fe3b02'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_230056_e377054e-2dca-47ef-8109-c17de5712a0d'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115259_1be03147-94f3-4a14-b352-019974b67a3a'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_40673_cb2e6a8f-a21f-44b1-bfce-7c5550dcf2a6'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115070_763631cf-edd6-454a-86c0-54d22be39e99'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_176798_a87f1f14-d697-4998-b921-4261f19c7156'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115007_0b9d7cfb-c9bb-4d0d-b1f3-2f1e1f498f68'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_176795_9d4a9c2b-1842-4d85-92d2-19ae7220ed79'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_176822_3c8aa301-b461-4161-8108-a3b293d88bcc'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_40726_b025723c-e7f4-4d80-bb7f-4f191c72280e'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115051_7c8c5e2e-5362-487a-a22e-5fa58bcaae55'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115270_0d85315e-f532-4cc3-87f1-314a3506d06c'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_40816_83444869-aa35-4fcc-827f-75c6d2c9ae34'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_40814_2a2a5299-fab4-4301-8189-6c158b317abf'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_114397_4d8f9fd1-d2e3-49a4-8a91-323e236d06e5'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115247_0832b504-001a-4586-8276-038964dbe9d2'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_114966_cdd59c2c-8847-4f69-bddc-f61469d5c025'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115113_cd13a77f-37b6-427c-b35c-02a38064cf36'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_1368626_f7f5b54f-f2b1-46c0-96a4-e4ca3b19fb67'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_42940_845b11e9-915d-419c-bd73-4021dd56b15a'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_42963_55e2ef3c-a314-4798-975f-f10b5bbf9491'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115079_1198953b-c158-464f-aaf1-5ec4147f0de4'", 'color("GREEN")'],
["${file_candidate_gmlid} === 'BID_115209_09b1b509-2f20-4153-ab57-7e9aa5a5769d'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115101_70f9ea2d-cc9e-465f-a3b9-fa8f2a7c0c1f'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115143_34bf8b47-173e-4331-92e2-8950d83a2225'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115210_6b29fa43-194e-4c32-a1c6-a919aa83ce2a'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115231_c98ababe-b3a3-46ea-8abd-ecc15a5b69e3'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115269_72ea93e7-ea27-47a6-ba9c-d2d02b037583'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176567_41bbe8f5-70c3-4f2d-8add-7438496b55fb'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_222199_374954de-99fc-49c8-872d-c626c3612ab1'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_41621_32c4b83d-a8e4-41f2-a299-77588060f06c'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_41969_a0918d34-5e36-4c6a-9c7e-8f56f646009e'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115015_c8e51621-f057-44b1-922a-84208ce95bca'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_1445300_6886841e-068a-4f8e-8916-edba97358df9'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176836_4e9a18c5-1133-4273-bb05-23d58ed5a17b'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_41576_974fcad2-de74-4e8b-925a-76f651045feb'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176796_13d7e810-d02e-450d-af81-cf296aa44981'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_114393_217c5812-fac6-4dc8-9bd5-0312eccc065d'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_114432_58a6f666-78e6-4c35-923d-ae1a7f79281b'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115080_8214d917-76d0-4762-bfd2-b2fbaa21674c'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_195732_10024501-0786-449c-9e3e-8b762611f50b'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_199508_9382c8df-3338-44bb-9573-f725830604bd'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_113562_61ac152f-9d3f-46d9-9281-2698b4550f4d'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_114398_22645422-1754-4d65-9e3f-d7d8a48230bf'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_114414_89143c47-3b86-4d05-9190-fe3a2c2a3ca6'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115089_49cbb844-7ce2-4987-ba75-3f994dd4e118'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115106_0122cc90-b473-48dd-8f58-2313326d51d8'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115120_b81dd9aa-b34c-4e67-bb77-4d6b62c6e574'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_1445298_3f4988c1-5b63-4f55-a1d6-bdd94b255cd9'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_1445299_65b6e983-a64d-43d1-9bfa-6a28ebc953f9'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176848_35a8eb5d-9e4a-4d6d-a0d8-8aed8a497fe3'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_199521_99cb4fa2-6db9-4aa0-88f0-e1c22ca85872'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_209451_c54b96bb-f5d2-4d93-9abd-847380081b28'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_114925_5d80c1e5-1874-4281-97e4-665b6decc12b'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_43831_04dc18b0-2049-463b-9fa8-483ce34a53f9'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176785_7e72c221-6868-4cec-be69-7ecb97d9c863'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115094_f5c766ef-9421-49ad-b055-6506c2ad11b1'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115208_21224cf2-ed42-408e-bf26-8e4904713594'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115111_b8a30629-c706-4467-aea2-b115201fae58'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_217699_a6416502-0fd7-4182-be43-d74183ad4a69'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_43275_ca6b85f0-e28b-4991-a87c-2779571a4486'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_198235_cc1c000d-5d3f-45e0-a85a-5355a036dbfb'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_1500671_ab0adf2e-185b-490f-8eb1-3b5a0beabd53'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_113557_ecf2b4af-2765-4d1b-a1c7-0182c7e042fd'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_114394_aded5344-1ada-45b6-bc6d-cf311bbc8175'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115018_0fbaf925-91eb-4b76-94b2-60cfe415b1df'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115085_13b7cb86-2af5-4034-b934-7df7ab729fc8'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115214_6e5e0ef9-84f0-4a43-b30f-aed39675c8cb'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_1500586_fa973669-3152-4c1d-987f-40f03a70bdd1'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176784_ac771b8a-0a21-49b7-8ad6-8c506449911a'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176834_c0ce066c-dfcf-409a-a149-2cec27ec5f7f'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115083_44d915d8-9caa-427a-96be-0bb223f20cc1'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115243_4cc3d2da-b07d-4cb6-b1fc-f029ac4c4e57'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_114395_9a5a35e2-7fea-4dae-814f-ef65f70c4c9d'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_114435_40f613a2-bf20-4491-a4ad-f29dd6437522'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_114985_e3fbaf17-6eaf-48cd-8a82-a218afd4d9ee'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115084_3a953d64-632d-4602-97ee-0fb109b1a6df'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115096_77f6b8fc-59f3-4cfd-a53a-f612d98c2fd4'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115115_c4145445-be2f-4ba9-b12b-cc4e6c959152'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115232_91f93b1d-e9b1-43d2-a5b2-00e571813b89'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_1265060_1299d3b0-7460-4fa0-a030-eb71e525feaa'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176779_936589f1-0727-4918-8605-a6a88b6523df'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176903_6259261b-7975-4cf7-8b60-1bd9aa58a2ac'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_43818_6447b0ee-c2ab-4528-9649-4bfd4fcbe382'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115201_97c80243-9280-4e90-8f0d-cc3bff91a3a9'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_198243_3d66733e-da01-4236-9efe-b19346c9fcbf'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_1265063_e339f5ab-c715-471b-97cb-bd70efdc28b9'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_1500673_59403c37-ee97-4941-a564-563256a7b7cb'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_114392_0788d8b2-285f-4df5-988c-ba22b67b7ff8'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115055_4f98706f-407c-4fb8-a56d-94d00bd53f7e'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115058_6a0a8978-64eb-4fcf-be15-299ddeb80b93'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115059_b96b78d1-1b89-4961-9f4c-dba2722883e7'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115060_cb305761-36b5-436e-ada6-0f065f17f031'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115071_3cd5fd47-20de-4a87-a822-72a0311c9e6c'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115107_7b0dbe25-4a6c-431a-baef-39584dcc95aa'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115117_4910b1b2-33c1-4359-b140-5604fc878181'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115127_e5aabf3e-bf32-45d3-87a6-c939696f64aa'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115133_99a1dda4-2e3a-48d1-9a48-ff53b5b85f05'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115239_97f673ba-574b-4d59-a3f0-a516264e2955'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115245_ca9deb66-07db-4e9e-ad22-64b70c8783cf'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_170885_3349fdec-734c-40d5-8987-f0db4e6e0739'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176755_9c71e17b-e6cd-43d9-a9b3-f8d5b5b9a06d'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176767_c78a0889-691d-47a8-a429-706be1c9af5e'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176768_19f0879f-9f3c-4727-a969-9feacea3b2d7'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176830_d4a0bd18-6635-40a3-889a-9a8cb3523437'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176839_553b5c6f-4461-4fe8-880b-9a3ce85a440a'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_212123_bd8aea02-0550-4f02-aa3b-fc27c0791b2b'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_218079_133581f6-97fc-4a9d-bf1f-78ab5cb09d42'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_240635_efde5dd4-816c-474b-b9a6-fca0ec1770c8'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_240636_57dd15c5-d856-4eb4-bd3f-68df4dd4d823'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176766_331bd761-027c-47e6-b64d-3da59338b273'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176841_16cd0b34-97df-4da2-bd17-00ca06f8b282'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_198267_5f192e6a-0875-4806-8cb6-a06da23cec77'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115025_d27c951f-35e3-496e-b3d0-f7dab37c1094'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115054_76d2481f-20a3-4592-994e-f02b0240eade'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115062_796c63d8-d8a8-4c26-a62f-e97a52aa85c5'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_115261_8286875a-63bd-40a6-92a1-c10830f98fdf'", 'color("RED")'],
["${file_candidate_gmlid} === 'BID_176292_4f3d95fc-bcf5-4bf3-9615-eb814c4fe0d7'", 'color("RED")'],
                    ['true', 'color("blue",0)']
                ]
            },
    show: true
});
var setHeatStyle = function () {
    tileset.style = heatDemandStyle;
    $("#Legend_pv").show(500);
}

// tileset.style = new Cesium.Cesium3DTileStyle({
//     color: {
//         conditions: [
//             ['${Height} >= 100', 'color("purple", 0.5)'],
//             ['${Height} >= 50', 'color("red")'],
//             ['true', 'color("blue")']
//         ]
//     },
//     show: '${Height} > 0',
//     meta: {
//         description: '"Building id ${id} has height ${Height}."'
//     }
// });

var zoomToTileset = function () {
    viewer.zoomTo(tileset);
}
