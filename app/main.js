Cesium.BingMapsApi.defaultKey = 'ApOW9LMkerqWIVSnFauilSeaZyp8df66byy1USCTjgTdMvhb4y1iAhEsUHQfCgzq'
// Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmOGQ1ODcwNC0zNDU1LTQ5ZGEtOWE4YS0zYzNmYTcxOWQxYTUiLCJpZCI6MTAxMywiaWF0IjoxNTI2NjM1Nzg4fQ.X43-3OHqmcwPP5qCE2NqSGJUsj5FGWgXAzitvrV0pwY';
Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmMGJkYzQwZS1lNTI2LTQ1N2UtYjEyNy1lMjJiYjg2YjIxYmQiLCJpZCI6MTcxNDQsInNjb3BlcyI6WyJhc3IiLCJnYyJdLCJpYXQiOjE1NzE3Mjc2Njl9.C_XxMaj0_rAu-ADAkpcjcdzcccEMZ-cdbcn4fi5qby8'

var viewer = new Cesium.Viewer('cesiumContainer', {
    terrainProvider: Cesium.createWorldTerrain(),

    baseLayerPicker: true,
    homeButton: true,
    scene3DOnly: true,
    terrainExaggeration: 0.5,
    navigationHelpButton: false,
    selectionIndicator: false,
    sceneModePicker: false,
    geocoder: true,
    shadows: false,

    // imageryProvider : new Cesium.ArcGisMapServerImageryProvider({
    //     url : '//services.arcgis.com/1lplwYilIlo008hQ/arcgis/rest/services/AccidentAugburgSHP_Density/MapServer'

    // }),
    // imageryProvider : new Cesium.IonImageryProvider({ assetId: 4 })
});
// Basic Set Up for the Cesium Globe
viewer.scene.globe.enableLighting = true;
//initial camera 
var homeCameraView = {
    destination: {
        x: 4138789.3229200086,
        y: 612789.6783915799,
        z: 4798201.190526457
    },
    orientation: {
        direction: {
            x: -0.8915645712611195,
            y: 0.44698338532571574,
            z: 0.07292783086544413
        },
        up: {
            x: 0.2518506964102406,
            y: 0.35548990831326327,
            z: 0.9001100776044587
        }
    }
};
// Add some camera flight animation options
homeCameraView.duration = 2.0;
homeCameraView.maximumHeight = 2000;
homeCameraView.pitchAdjustHeight = 2000;
homeCameraView.endTransform = Cesium.Matrix4.IDENTITY;
viewer.scene.camera.setView(homeCameraView);
viewer.homeButton.viewModel.command.beforeExecute.addEventListener(function (e) {
    e.cancel = true;
    viewer.scene.camera.flyTo(homeCameraView);
});
viewer.clock.shouldAnimate = true; // default00
viewer.clock.startTime = Cesium.JulianDate.fromIso8601("2018-11-24T12:00:00Z");
viewer.clock.stopTime = Cesium.JulianDate.fromIso8601("2018-11-24T18:00:00Z");
viewer.clock.currentTime = Cesium.JulianDate.fromIso8601("2018-11-24T12:00:00Z");
viewer.clock.multiplier = 0.5; // sets a speedup
viewer.clock.clockRange = Cesium.ClockRange.LOOP_STOP; // loop at the end
viewer.timeline.zoomTo(viewer.clock.startTime, viewer.clock.stopTime); // set visible range

var tileset, tileset_door, tileset_roof, tileset_ground;
var tileset_wuestenrot;
var heightOffset = 49;
var addLayers3DT_Sample = function () {
    // CesiumStyleChoice.style.display = "block";
    // $("#CesiumStyleChoice").show("1000");
    try {
        tileset = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: './Datas_3D/3DTile_out_clamp/CityModel/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }

    // Styling the Defalut style
    var transparentStyle = new Cesium.Cesium3DTileStyle({
        color: "color('WHITE', 1)",
        show: true
    });
    tileset.style = transparentStyle;
};

var addLayersWuestenrot = function () {
    // CesiumStyleChoice.style.display = "block";
    // $("#CesiumStyleChoice").show("1000");
    try {
        tileset_wuestenrot = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: './Datas_3D/wuestenrot_vcs_out_final/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }

    // Styling the Defalut style
    var transparentStyle = new Cesium.Cesium3DTileStyle({
        color: "color('WHITE', 1)",
        show: true
    });
    tileset_wuestenrot.style = transparentStyle;
};
addLayersWuestenrot();

var addLayers3DT_Separate = function () {
    // Wall
    try {
        tileset = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: './Datas_3D/3DTile_out_separate/WallSurface/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    //Adjust a tilesetDD's height from the globe's surface.
    tileset.readyPromise.then(function () {
        var boundingSphere = tileset.boundingSphere;
        var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
        var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
        var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
        var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
        tileset.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
    });
    // Styling the Defalut style
    var transparentStyle = new Cesium.Cesium3DTileStyle({
        color: "color('WHITE', 1)",
        show: true
    });
    tileset.style = transparentStyle;

    // Door
    try {
        tileset_door = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: './Datas_3D/3DTile_out_separate/Door/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    //Adjust a tilesetDD's height from the globe's surface.
    tileset_door.readyPromise.then(function () {
        var boundingSphere = tileset_door.boundingSphere;
        var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
        var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
        var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
        var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
        tileset_door.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
    });
    // Styling the Defalut style
    var transparentStyle = new Cesium.Cesium3DTileStyle({
        color: "color('BLUE', 1)",
        show: true
    });
    tileset_door.style = transparentStyle;

    // Roof
    try {
        tileset_roof = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: './Datas_3D/3DTile_out_separate/RoofSurface/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    //Adjust a tilesetDD's height from the globe's surface.
    tileset_roof.readyPromise.then(function () {
        var boundingSphere = tileset_roof.boundingSphere;
        var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
        var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
        var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
        var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
        tileset_roof.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
    });
    // Styling the Defalut style
    var transparentStyle = new Cesium.Cesium3DTileStyle({
        color: "color('RED', 1)",
        show: true
    });
    tileset_roof.style = transparentStyle;

    // ground
    try {
        tileset_ground = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: './Datas_3D/3DTile_out_separate/GroundSurface/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    //Adjust a tilesetDD's height from the globe's surface.
    tileset_ground.readyPromise.then(function () {
        var boundingSphere = tileset_ground.boundingSphere;
        var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
        var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
        var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
        var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
        tileset_ground.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
    });
    // Styling the Defalut style
    var transparentStyle = new Cesium.Cesium3DTileStyle({
        color: "color('BROWN', 1)",
        show: true
    });
    tileset_ground.style = transparentStyle;

    // window
    try {
        tileset_window = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: './Datas_3D/3DTile_out_separate/Window/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    //Adjust a tilesetDD's height from the globe's surface.
    tileset_window.readyPromise.then(function () {
        var boundingSphere = tileset_window.boundingSphere;
        var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
        var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
        var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
        var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
        tileset_window.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
    });
    // Styling the Defalut style
    var transparentStyle = new Cesium.Cesium3DTileStyle({
        color: "color('AQUA', 1)",
        show: true
    });
    tileset_window.style = transparentStyle;
}

var tileset_ND;
var addLayers3DPS_Niederhalle = function () {
    // CesiumStyleChoice.style.display = "block";
    // $("#CesiumStyleChoice").show("1000");
    try {
        tileset_ND = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: 'http://81.169.187.7:9000/ext_assets/3dtiles/niedernhall/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }

    // Styling the Defalut style
    var transparentStyle = new Cesium.Cesium3DTileStyle({
        color: "color('WHITE', 1)",
        show: true
    });
    tileset_ND.style = transparentStyle;
};
addLayers3DPS_Niederhalle();

//Tileset Stoeckach
// var tileset_SK;
// var addLayers3DPS_SK = function () {
//     // CesiumStyleChoice.style.display = "block";
//     // $("#CesiumStyleChoice").show("1000");
//     try {
//         tileset_SK = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
//             url: 'http://81.169.187.7:8083/Assets/Stoeckach_070519_3DTiles/geo-toolbox/tileset.json',
//             maximumScreenSpaceError: 8 // default value
//         }));

//     } catch (err) {
//         console.log('->  add 3DTiles failed!\n' + err);
//     }
//     tileset_SK.readyPromise.then(function () {
//         var boundingSphere = tileset_SK.boundingSphere;
//         var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
//         var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
//         var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
//         var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
//         tileset_SK.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
//     });
//     // Styling the Defalut style
//     var transparentStyle = new Cesium.Cesium3DTileStyle({
//         color: "color('WHITE', 1)",
//         show: true
//     });
//     tileset_SK.style = transparentStyle;
// };
// addLayers3DPS_SK();

//Tileset Sejong
var tileset_sejong;
var addLayerSejong= function () {
    // CesiumStyleChoice.style.display = "block";
    // $("#CesiumStyleChoice").show("1000");
    try {
        tileset_sejong = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: './Datas_3D/3DTile_Sejong/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    tileset_sejong.readyPromise.then(function () {
        // var boundingSphere = tileset_sejong.boundingSphere;
        // var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
        // var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
        // var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0);
        // var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
        // tileset_sejong.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
        viewer.zoomTo(tileset_sejong);
    });
    // // Styling the Defalut style
    // var transparentStyle = new Cesium.Cesium3DTileStyle({
    //     color: "color('WHITE', 1)",
    //     show: true
    // });
    // tileset_sejong.style = transparentStyle;
};


//Tileset Point Cloud
var tileset_pointcloud_windSim;
var addLayers3DPS_pointcloud_windsim = function () {
    // CesiumStyleChoice.style.display = "block";
    // $("#CesiumStyleChoice").show("1000");
    try {
        tileset_pointcloud_windSim = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: 'http://193.196.37.89:8092/AssetsHFT/ADAC/tileset.json',
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    tileset_pointcloud_windSim.readyPromise.then(function () {
        var boundingSphere = tileset_pointcloud_windSim.boundingSphere;
        var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
        var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
        var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
        var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
        tileset_pointcloud_windSim.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
    });
// Styling the Defalut style
    var pointcloudStyle = new Cesium.Cesium3DTileStyle({
        pointSize : '3.0',
        show: true
    });
    tileset_pointcloud_windSim.style = pointcloudStyle;
};
addLayers3DPS_pointcloud_windsim();

//Tileset Stoeckach
// var tileset_Stuttgart_Orange;
// var addLayers_Stuttgart_Orange = function () {
//     // CesiumStyleChoice.style.display = "block";
//     // $("#CesiumStyleChoice").show("1000");
//     try {
//         tileset_Stuttgart_Orange = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
//             url: './Datas_3D/Stuttgart_Orange_3DTiles/tileset.json',
//             maximumScreenSpaceError: 8 // default value
//         }));

//     } catch (err) {
//         console.log('->  add 3DTiles failed!\n' + err);
//     }
//     tileset_Stuttgart_Orange.readyPromise.then(function () {
//         var boundingSphere = tileset_Stuttgart_Orange.boundingSphere;
//         var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
//         var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
//         var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
//         var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
//         tileset_Stuttgart_Orange.modelMatrix = Cesium.Matrix4.fromTranslation(translation);
//     });
//     // // Styling the Defalut style
//     // var transparentStyle = new Cesium.Cesium3DTileStyle({
//     //     color: "color('WHITE', 1)",
//     //     show: true
//     // });
//     // tileset_Stuttgart_Orange.style = transparentStyle;
// };
// addLayers_Stuttgart_Orange();

// addLayers3DT();
// addLayers3DT_Sample();
addLayers3DT_Separate();


var zoomToTileset = function () {
    viewer.zoomTo(tileset);
}

var zoomToWR = function () {
    viewer.zoomTo(tileset_wuestenrot);
}

var zoomToND = function () {
    viewer.zoomTo(tileset_ND);
}

var zoomToStuttgart = function () {
    viewer.zoomTo(tileset_Stuttgart_Orange);
}