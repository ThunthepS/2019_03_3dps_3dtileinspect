//////////////////////////////////
// Test to add Geojson Request////
//////////////////////////////////
var routeEntities = [];
var lastRouteResult;
var routeOpenMap;
var routeService_ii = 0;
var routeService = function () {
    if (routeService_ii>0){
        for (let i = 0; i < routeService_ii; i++) {
            routeEntities[i].show = false;
        }
    }
    // viewer.dataSources.removeAll();
    // entity.label.show = true;
    var geojsonOptions = {
        stroke: new Cesium.Color(0, 1, 0, 1),
        fill: new Cesium.Color(0, 1, 0, 1),
        strokeWidth: 3,
        clampToGround: true
    };
    const apiK = '58d904a497c67e00015b45fc1512797ef30249a395e53c3b97a807e7';
    var fromPo = [document.getElementById('RouteFromLong').value, document.getElementById('RouteFromLat').value];
    var toPo = [document.getElementById('RouteToLong').value, document.getElementById('RouteToLat').value];
    var fromToPo = fromPo[0] + '%2C' + fromPo[1] + '%7C' + toPo[0] + '%2C' + toPo[1];
    var SENSOR_API_BASE_URL = 'https://api.openrouteservice.org/directions?api_key=' + apiK;
    var SENSOR_API_FINAL_URL =    '&coordinates='+fromToPo +'&profile=cycling-electric&geometry_format=geojson' + 
                                '&elevation=true' + '&instructions_format=html';
    var finalURL = SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL;
    $.getJSON(finalURL, function (geotesta) {
        //console.log(data.value[0].result);
        console.log("request at:" + finalURL);
        console.log( "request route success" );
        var routePromise = Cesium.GeoJsonDataSource.load(geotesta["routes"][0]["geometry"], geojsonOptions);
        lastRouteResult = geotesta;
        //var neighborhoods;
        routePromise.then(function (dataSource) {
            // Add the new data as entities to the viewer
            routeOpenMap = viewer.dataSources.add(dataSource);
            routeEntities[routeService_ii] = dataSource.entities.values[0];
            routeEntities[routeService_ii].name = "Route";
            routeEntities[routeService_ii].id = "RouteOpenMapResult";
            routeEntities[routeService_ii].description =
                "Distance : " + geotesta["routes"][0]["summary"]["distance"] + "m<br>" +
                "Duration : " + geotesta["routes"][0]["summary"]["duration"] + "sec<br>" +
                "Ascent : " + geotesta["routes"][0]["summary"]["ascent"] + "m<br>" +
                "Descent: " + geotesta["routes"][0]["summary"]["descent"] + "m<br>"
                ;
            routeEntities[routeService_ii].label = {
                text: "Route",
                showBackground: true,
                scale: 0.6,
                font: '14px monospace',
                horizontalOrigin: Cesium.HorizontalOrigin.CENTER,
                verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
                //distanceDisplayCondition : new Cesium.DistanceDisplayCondition(10.0, 8000.0),
                disableDepthTestDistance: Number.POSITIVE_INFINITY,
                backgroundPadding: new Cesium.Cartesian2(20, 8),
                pixelOffset: new Cesium.Cartesian2(50, -50),
                backgroundColor: new Cesium.Color(0.0, 0, 0.0, 0.3)
            };
            routeService_ii +=1;
        });
    });

};

var HideRoute = function () {
    // viewer.dataSources.removeAll();
    viewer.entities.remove(RountPinPoint_A);
    viewer.entities.remove(RountPinPoint_B);
}


var freeModeElement = document.getElementById('freeMode');
var TrackingMode = document.getElementById('trackMode');

// Create a follow camera by tracking the drone entity
function setViewMode() {
    if (TrackingMode.checked) {
        routeDatasource.viewFrom = new Cesium.Cartesian3(-450, -450, 500);
        viewer.trackedEntity = routeDatasource;
    } else if (freeModeElement.checked) {
        viewer.trackedEntity = undefined;
        viewer.scene.camera.flyTo(homeCameraView);
    } else {
        viewer.trackedEntity = undefined;
        viewer.scene.camera.flyTo(homeCameraView);
    }
}

freeModeElement.addEventListener('change', setViewMode);
TrackingMode.addEventListener('change', setViewMode);

viewer.trackedEntityChanged.addEventListener(function () {
    if (viewer.trackedEntity === routeDatasource) {
        freeModeElement.checked = false;
        TrackingMode.checked = true;
    }
});