//////////////////////////////////
// A Function to trigger the menu////
//////////////////////////////////
var openRTEI = function () {
    if (RTEI.style.display === 'none') {
        // RTEI.style.display = "block";
        // HEU.style.display = "none";
        // DrawBB.style.display = "none";
        // MapP.style.display = "none";
        // CamMe.style.display = "none";
        // $("#RTEI").show("1000");
        // $("#HEU").hide("1000");
        // $("#DrawBB").hide("1000");
        $("#MapP").hide("1000");
        // $("#CamMe").hide("1000");
    } else {
        //RTEI.style.display = "none";
        $("#RTEI").hide("1000");
    }

}
var openHEU = function () {
    if (HEU.style.display === 'none') {
        $("#RTEI").hide("1000");
        $("#HEU").show("1000");
        // $("#DrawBB").hide("1000");
        $("#MapP").hide("1000");
        $("#CamMe").hide("1000");
    } else {
        $("#HEU").hide("1000");
    }
}
var openStat = function () {
    if (CamMe.style.display === 'none') {
        $("#RTEI").hide("1000");
        $("#HEU").hide("1000");
        $("#DrawBB").hide("1000");
        $("#MapP").hide("1000");
        $("#CamMe").show("1000");
    } else {
        $("#CamMe").hide("1000");
    }
}
var open3DrawBB = function () {
    if (DrawBB.style.display === 'none') {
        // $("#RTEI").hide("1000");
        // $("#HEU").hide("1000");
        // $("#DrawBB").show("1000");
        $("#MapP").hide("1000");
        // $("#CamMe").hide("1000");
    } else {
        $("#DrawBB").hide("1000");
    }
}
var openMapP = function () {
    if (MapP.style.display === 'none') {
        // $("#RTEI").hide("1000");
        // $("#HEU").hide("1000");
        // $("#DrawBB").hide("1000");
        $("#MapP").show("1500");
        $("#menuCircleUp").show();
        $("#menuCircleDown").hide();

        // $("#CamMe").hide("1000");
    } else {
        $("#MapP").hide("1500");
        $("#menuCircleUp").hide();
        $("#menuCircleDown").show();
    }
}
openMapP();
var displayMenu = function () {
    if (menu.style.display === 'none') {
        //menu.style.display = "block";
        $("#menu").fadeIn("3000");
    } else {
        //menu.style.display = "none"
        $("#menu").fadeOut("3000");
    }
}


var layerActivate = function () {
    if(typeof layerpart == "undefined") {
        //.... no layerpart
    } else {
        if (layerpart.style.display === 'none') {
        $("#layerpart").show("1000");
        $("#layerCircleUp").show();
        $("#layerCircleDown").hide();
    } else {
        $("#layerpart").hide("1000");
        $("#layerCircleDown").show();
        $("#layerCircleUp").hide();
    }
    }
}
layerActivate();
$(document).ready(function () {
    $('#Cesium3DTilesCheck').on('change', function () {

        if ($("#Cesium3DTilesCheck").prop("checked") == true) {
            // addLayers3DT();
            tileset.show = true;
            console.log("3D-Tile: Check true");
        } else {
            tileset.show = false;
            console.log("3D-Tile: Check false");
        }
    });
    $('#Cesium3DTilesCheck_roof').on('change', function () {

        if ($("#Cesium3DTilesCheck_roof").prop("checked") == true) {
            // addLayers3DT();
            tileset_roof.show = true;
            console.log("3D-Tile: Check true");
        } else {
            tileset_roof.show = false;
            console.log("3D-Tile: Check false");
        }
    });
    $('#Cesium3DTilesCheck_door').on('change', function () {

        if ($("#Cesium3DTilesCheck_door").prop("checked") == true) {
            // addLayers3DT();
            tileset_door.show = true;
            console.log("3D-Tile: Check true");
        } else {
            tileset_door.show = false;
            console.log("3D-Tile: Check false");
        }
    });
    $('#Cesium3DTilesCheck_ground').on('change', function () {

        if ($("#Cesium3DTilesCheck_ground").prop("checked") == true) {
            // addLayers3DT();
            tileset_ground.show = true;
            console.log("3D-Tile: Check true");
        } else {
            tileset_ground.show = false;
            console.log("3D-Tile: Check false");
        }
    });
    $('#Cesium3DTilesCheck_window').on('change', function () {

        if ($("#Cesium3DTilesCheck_window").prop("checked") == true) {
            // addLayers3DT();
            tileset_window.show = true;
            console.log("3D-Tile: Check true");
        } else {
            tileset_window.show = false;
            console.log("3D-Tile: Check false");
        }
    });

});
