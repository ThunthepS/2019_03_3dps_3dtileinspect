viewer.scene.fog.enabled = false;
// var igdcredit = new Cesium.Credit('Fraunhofer IGD', 'IGD.png', 'http://www.igd.fraunhofer.de/');
// viewer.scene.frameState.creditDisplay.addDefaultCredit(igdcredit);

var oldstyling = null;
var dragging = undefined;
var drawing = false;
var handler = new Cesium.ScreenSpaceEventHandler(viewer.canvas);
var rectVisible = false;
var Pickers_3DTile_Activated = true;

function handleResponse(xml, bbox, layer) {
    var xmlDoc = xml.responseXML;
    // The url = Service Address of Provider
    var url = "http://tb13.igd.fraunhofer.de:8083" + xmlDoc.getElementsByTagName("address")[0].childNodes[0].nodeValue;
    // loadTileset(url, bbox);

    // document.getElementById("toolbar1").style.display = "inline";
    // if (layer === "NY_LoD2"){
    // 	document.getElementById("stylingBox").style.visibility = "hidden";
    // 	oldstyling = null;
    // }
    // else if (layer === "berlin"){
    // 	document.getElementById("stylingBox").style.visibility = "hidden";
    // 	oldstyling = null;
    // }
    // else {
    // 	document.getElementById("stylingBox").style.visibility = "visible";
    // }
}

function requestGetCapabilities() {
    var requestUri = "http://tb13.igd.fraunhofer.de:8082/3dps?SERVICE=3DPS&ACCEPTVERSIONS=1.0&REQUEST=GetCapabilities";
    // Make request
    // Asynchron request
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {

        if (xhttp.readyState === 4 && xhttp.status === 200) {
            // get html element and add information
            var content = document.getElementById('window');
            content.innerHTML = '<textarea rows="30" cols="80" style="background: rgba(70, 72, 72, 0.65); border-radius: 20px; border:none; color:white;">' + xhttp.responseText + '</textarea>';
            $("#CapPopup").show("1500");
            // var dialog = document.getElementById('dialog');
            // dialog.style.visibility = "visible";
        } else if (xhttp.readyState === 4) {
            console.log("Could not reach 3dps server.");
        }
    };
    xhttp.open("GET", requestUri, true);
    xhttp.send();

}
var currentLayer;
//Joe part here -- link the function to html
function createRequestNY() {
    currentLayer = "NY_LoD2";
    var bbox = "";
    createRequest(currentLayer, bbox)
}

function drawBounding() {
    rectVisible = true;
    drawing = false;
    Pickers_3DTile_Activated = false;
    toggleRectangleAndPins(true);
}

function hideBounding() {
    drawing = false;
    rectVisible = false;
    Pickers_3DTile_Activated = true;
    toggleRectangleAndPins(false);
}

function createRequestMH() {
    currentLayer = "Manhattan";
    var bbox = "";
    createRequest(currentLayer, bbox)
}

function createRequestMH_BB() {
    currentLayer = "Manhattan";
    var degreeArray = getPinPositionsAsDegreeArray();
    var bbox = "&bbox=" + String(degreeArray[0]) + "%2C" + String(degreeArray[1]) + "%2C" + String(degreeArray[2]) + "%2C" + String(degreeArray[3]);
    createRequest(currentLayer, bbox)
}



var postServerResult;

function createRequest(layer, bbox) {
    // layer has to be "newyork" or "manhattan"
    // bbox has to be "west,south,east,north"
    // adress has to be adjusted
    var requestUriOld = "http://tb13.igd.fraunhofer.de:8082/3dps?SERVICE=3DPS&VERSION=1.0&REQUEST=GetScene&LAYERS=" + layer + "&FORMAT=text/html&CRS=EPSG:4326" + bbox;
    if (bbox === "") {
        var requestUri = "http://steinbeis-3dps.eu:8080/tb14/wfs3/3D_CityModel_manhattan/collections/buildings/scene?format=application%2Fjson&bbox=-74.00635826977239%2C40.71778771238832%2C-73.97393297660074%2C40.75070138933127";
        ajaxPostRequest(requestUri, bbox);
        console.log("Request URI on the server (Full Area): " + requestUri);
    } else {
        try {


            var requestUri = "http://steinbeis-3dps.eu:8080/tb14/wfs3/3D_CityModel_manhattan/collections/buildings/scene?format=application%2Fjson" + bbox;
            ajaxPostRequest(requestUri, bbox);
            console.log("Request URI on the server : " + requestUri);
            console.log("With bounding box :" + bbox)
        } catch (err) {
            console.log("Error sending request with the BBOX")
        }
    }
}

function ajaxPostRequest(requestURI, bbox) {
    $.ajax({
        url: "/3dpsRequest",
        dataType: 'json',
        type: 'post',
        contentType: 'application/json',
        // async: false,
        data: JSON.stringify({
            "url": requestURI
        }),
        success: function (result) {
            // postServerResult = result;
            console.log("----3DPS Successfully load----");
            console.log(">> Start loading tileset " + result);
            postServerResult = result;
            loadTileset(result[0], bbox);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // console.log("textStatus: " + textStatus);
            console.log("XMLHttpRequest: " + XMLHttpRequest);
            console.log("errorThrown: " + errorThrown);
            console.log("--AJAX Post to 81.169.187.7 Iss--");
            console.log(">> Start loading Temp-Tileset " + result);
            loadTileset(result[0], bbox);
        }
    });
}

var tileset;

function loadTileset(tileurl, bbox) {
    reset();
    console.log(">> Start loading tileset : URL" + tileurl);
    try {
        tileset = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            // url: "http://localhost:8080/tileset.json",
            url: "Data/tileset_temp.json",
            dynamicScreenSpaceError: true,
            dynamicScreenSpaceErrorDensity: 0.00278,
            dynamicScreenSpaceErrorFactor: 4.0,
            dynamicScreenSpaceErrorHeightFalloff: 0.25,

            // proxy : new Cesium.DefaultProxy('/proxy/'),
            // maximumNumberOfLoadedTiles: 3,
            // maximumScreenSpaceError: 8
        }));
    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    Cesium.when(tileset.readyPromise).then(function (tileset) {
        var degreeArray = getPinPositionsAsDegreeArray();
        tileset.maximumScreenSpaceError = 30;
        active3DTilePicker();
        if (bbox === "") {
            var boundingSphere = tileset.boundingSphere;
            viewer.camera.flyToBoundingSphere(boundingSphere, new Cesium.HeadingPitchRange(0, -2.0, 0));
            console.log("--Load tileSet.json Successfully!--");
        } else {
            viewer.camera.flyTo({
                destination: Cesium.Rectangle.fromDegrees(degreeArray[0], degreeArray[1], degreeArray[2], degreeArray[3])
            });
        }
    });
}

function TilesetStyle_Height() {
    if (Cesium.defined(tileset)) {

        var heightStyle = new Cesium.Cesium3DTileStyle({
            color: {
                conditions: [
                    ["${MeasuredHeight} >= 300", "rgb(45, 0, 75)"],
                    ["${MeasuredHeight} >= 200", "rgb(102, 71, 151)"],
                    ["${MeasuredHeight} >= 100", "rgb(170, 162, 204)"],
                    ["${MeasuredHeight} >= 50", "rgb(224, 226, 238)"],
                    ["${MeasuredHeight} >= 25", "rgb(252, 230, 200)"],
                    ["${MeasuredHeight} >= 10", "rgb(248, 176, 87)"],
                    ["${MeasuredHeight} >= 5", "rgb(198, 106, 11)"],
                    ["true", "rgb(127, 59, 8)"]
                ]
            },
            meta: {
                description: '"Building id ${id} has height ${Height}."'
            }
        });
        tileset.style = heightStyle;
    } else {
        alert("Please, load 3D Tile before styling");
    }
}

function TilesetStyle_Heat() {
    if (Cesium.defined(tileset)) {

        var heatStyle = new Cesium.Cesium3DTileStyle({
            color: {
                conditions: [
                    ["(${heat} === '')", "color('#6495ED')"],
                    ["(${heat} >= 150.0)  ", "color('#FF0000')"],
                    ["(${heat} >= 130.0)", "color('#FF8000')"],
                    ["(${heat} >= 120.0) ", "color('#E2BF0C')"],
                    ["(${heat} >= 110.0) ", "color('#FFFF00')"],
                    ["(${heat} >= 100.0) ", "color('#B1E309')"],
                    ["(${heat} >= 50.0)", "color('#008000')"],
                    ["(${heat} >= 80.0)", "color('#86E307')"],
                    ["(${heat} >= 70.0)", "color('#67B200')"],
                    ["true", "color('#6495ED')"]

                ]
            }
        });
        tileset.style = heatStyle;
    } else {
        alert("Please, load 3D Tile before styling");
    }
}

function TilesetStyle_PLUTO_building_area() {
    if (Cesium.defined(tileset)) {

        var buildingAreaStyle = new Cesium.Cesium3DTileStyle({
            color: {
                conditions: [
                    // ["(${PLUTO_building_area} === '')", "color('#6495ED')"],
                    ["(${PLUTO_building_area} >= 300000.0)  ", "color('#FF0000')"],
                    ["(${PLUTO_building_area} >= 150000.0)", "color('#FF8000')"],
                    ["(${PLUTO_building_area} >= 75000.0) ", "color('#E2BF0C')"],
                    ["(${PLUTO_building_area} >= 37500.0) ", "color('#FFFF00')"],
                    ["(${PLUTO_building_area} >= 18000.0) ", "color('#B1E309')"],
                    ["(${PLUTO_building_area} >= 9000.0)", "color('#008000')"],
                    ["(${PLUTO_building_area} >= 4500.0)", "color('#86E307')"],
                    ["(${PLUTO_building_area} >= 2000.0)", "color('#67B200')"],
                    ["true", "color('#6495ED')"]
                ]
            }
        });
        tileset.style = buildingAreaStyle;
    } else {
        alert("Please, load 3D Tile before styling");
    }
}

function TilesetStyle_Simple() {
    if (Cesium.defined(tileset)) {
        var SimpleStyle = new Cesium.Cesium3DTileStyle({
            color: "color('white')",
        });
        tileset.style = SimpleStyle;
    } else {
        alert("Please, load 3D Tile before styling");
    }
}


function reset() {
    if (Cesium.defined(tileset)) {
        viewer.scene.primitives.remove(tileset);
    }
}

// part for drawing BB
var pinBuilder = new Cesium.PinBuilder();

var pin1 = viewer.entities.add({
    position: Cesium.Cartesian3.fromDegrees(74.018997336757550443, 40.69846622682742776, 0.0),
    billboard: {
        image: new Cesium.ConstantProperty(pinBuilder.fromText("1", Cesium.Color.BLUE, 48)),
        verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
        show: false
    }
});

var pin2 = viewer.entities.add({
    position: Cesium.Cartesian3.fromDegrees(73.969593360166612683, 40.758282705492597131, 0.0),
    billboard: {
        image: new Cesium.ConstantProperty(pinBuilder.fromText("2", Cesium.Color.BLUE, 48)),
        verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
        show: false
    }
});
var bbRectangle = viewer.entities.add({
    rectangle: {
        coordinates: new Cesium.CallbackProperty(getPinPositions, false),
        material: Cesium.Color.BLUE.withAlpha(0.2),
        outline: true,
        outlineColor: Cesium.Color.WHITE,
        extrudedHeight: 2,
        show: false
    }
});

function toggleRectangleAndPins(show) {
    pin1.billboard.show = show;
    pin2.billboard.show = show;
    bbRectangle.rectangle.show = show;
}

handler.setInputAction(function (click) {
    if (rectVisible) {
        drawing = true;
        var position = getMousePosition(click.position);
        pin1.position._value = position;
        viewer.scene.screenSpaceCameraController.enableRotate = false;
    }
}, Cesium.ScreenSpaceEventType.LEFT_DOWN);

handler.setInputAction(function () {
    if (rectVisible && drawing) {

        drawing = false;
        rectVisible = false;
        viewer.scene.screenSpaceCameraController.enableRotate = true;

    }

}, Cesium.ScreenSpaceEventType.LEFT_UP);

// drag PINs
handler.setInputAction(function (movement) {
    if (rectVisible) {
        var position = getMousePosition(movement.endPosition);
        if (position !== -1) {
            pin2.position._value = position;
            if (!drawing) {
                pin1.position._value = position;
            }
        }
    }

}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

function getMousePosition(mousePosition) {
    var ray = viewer.camera.getPickRay(mousePosition);
    var position = viewer.scene.globe.pick(ray, viewer.scene);
    if (!Cesium.defined(position) || !(dragging !== undefined || rectVisible === true)) {
        return -1;
    }
    return position;
}

function getPinPositionsAsDegreeArray() {
    var carto1 = Cesium.Ellipsoid.WGS84.cartesianToCartographic(pin1.position.getValue(0));
    var carto2 = Cesium.Ellipsoid.WGS84.cartesianToCartographic(pin2.position.getValue(0));
    var lon1 = Cesium.Math.toDegrees(carto1.longitude);
    var lat1 = Cesium.Math.toDegrees(carto1.latitude);

    var lon2 = Cesium.Math.toDegrees(carto2.longitude);
    var lat2 = Cesium.Math.toDegrees(carto2.latitude);

    var smallerLat = 0;
    var biggerLat = 0;
    if (lat1 < lat2) {
        smallerLat = lat1;
        biggerLat = lat2;
    } else {
        smallerLat = lat2;
        biggerLat = lat1;
    }
    var smallerLon = 0;
    var biggerLon = 0;
    if (lon1 < lon2) {
        smallerLon = lon1;
        biggerLon = lon2;
    } else {
        smallerLon = lon2;
        biggerLon = lon1;
    }
    return [smallerLon, smallerLat, biggerLon, biggerLat];
}

function getPinPositions() {
    var degreeArray = getPinPositionsAsDegreeArray();
    var rectangle = Cesium.Rectangle.fromDegrees(degreeArray[0], degreeArray[1], degreeArray[2], degreeArray[3]);
    return rectangle;
}


// ==========================add part to select the building========================================================
// Information about the currently highlighted feature
function active3DTilePicker() {
    var highlighted = {
        feature: undefined,
        originalColor: new Cesium.Color()
    };
    // Information about the currently selected feature
    var selected = {
        feature: undefined,
        originalColor: new Cesium.Color()
    };

    // An entity object which will hold info about the currently selected feature for infobox display
    var selectedEntity = new Cesium.Entity();

    // Get default left click handler for when a feature is not picked on left click
    var clickHandler = viewer.screenSpaceEventHandler.getInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK);
    // Color a feature yellow on hover.
    viewer.screenSpaceEventHandler.setInputAction(function onMouseMove(movement) {
        if (Pickers_3DTile_Activated) {
            // If a feature was previously highlighted, undo the highlight
            if (Cesium.defined(highlighted.feature)) {
                highlighted.feature.color = highlighted.originalColor;
                highlighted.feature = undefined;
            }
            // Pick a new feature
            var picked3DtileFeature = viewer.scene.pick(movement.endPosition);
            if (!Cesium.defined(picked3DtileFeature)) {
                // nameOverlay.style.display = 'none';
                return;
            }
            // A feature was picked, so show it's overlay content
            // nameOverlay.style.display = 'block';
            // nameOverlay.style.bottom = viewer.canvas.clientHeight - movement.endPosition.y + 'px';
            // nameOverlay.style.left = movement.endPosition.x + 'px';
            var name = picked3DtileFeature.getProperty('name');
            if (!Cesium.defined(name)) {
                name = picked3DtileFeature.getProperty('OBJECTID');
            }
            // nameOverlay.textContent = name;
            // Highlight the feature if it's not already selected.
            if (picked3DtileFeature !== selected.feature) {
                highlighted.feature = picked3DtileFeature;
                Cesium.Color.clone(picked3DtileFeature.color, highlighted.originalColor);
                picked3DtileFeature.color = Cesium.Color.YELLOW;
            }
        }
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

    // Color a feature on selection and show metadata in the InfoBox.
    viewer.screenSpaceEventHandler.setInputAction(function onLeftClick(movement) {
        if (Pickers_3DTile_Activated) {
            // If a feature was previously selected, undo the highlight
            if (Cesium.defined(selected.feature)) {
                selected.feature.color = selected.originalColor;
                selected.feature = undefined;
            }
            // Pick a new feature
            var picked3DtileFeature = viewer.scene.pick(movement.position);
            if (!Cesium.defined(picked3DtileFeature)) {
                clickHandler(movement);
                return;
            }
            // Select the feature if it's not already selected
            if (selected.feature === picked3DtileFeature) {
                return;
            }
            selected.feature = picked3DtileFeature;
            // Save the selected feature's original color
            if (picked3DtileFeature === highlighted.feature) {
                Cesium.Color.clone(highlighted.originalColor, selected.originalColor);
                highlighted.feature = undefined;
            } else {
                Cesium.Color.clone(picked3DtileFeature.color, selected.originalColor);
            }
            // Highlight newly selected feature
            picked3DtileFeature.color = Cesium.Color.LIME;
            // Set feature infobox description
            var featureName = picked3DtileFeature.getProperty('OBJECTID');
            selectedEntity.name = "Building:" + featureName;
            selectedEntity.description = 'Loading <div class="cesium-infoBox-loading"></div>';
            viewer.selectedEntity = selectedEntity;
            selectedEntity.description = '<table class="cesium-infoBox-defaultTable"><tbody>' +
                //  '<tr><th>ID</th><td>' + picked3DtileFeature.getProperty('OBJECTID') + '</td></tr>' +
                '<tr><th>Measured Height</th><td>' + picked3DtileFeature.getProperty('MeasuredHeight') + '</td></tr>' +
                '<tr><th>Building Area</th><td>' + picked3DtileFeature.getProperty('PLUTO_building_area') + '</td></tr>' +
                '<tr><th>Year Built</th><td>' + picked3DtileFeature.getProperty('PLUTO_year_built') + '</td></tr>' +
                '<tr><th>Heat Demand</th><td>' + picked3DtileFeature.getProperty('heat') + '</td></tr>' +
                '<tr><th>Longitude</th><td>' + picked3DtileFeature.getProperty('Longitude') + '</td></tr>' +
                '<tr><th>Latitude</th><td>' + picked3DtileFeature.getProperty('Latitude') + '</td></tr>' +
                '</tbody></table>';
        }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
}