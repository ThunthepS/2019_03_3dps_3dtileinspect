// ==========================add part to select the building========================================================
var Pickers_3DTile_Activated = true;
var last_picked_3DTiles;
// Information about the currently highlighted feature
function active3DTilePicker() {
    var highlighted = {
        feature: undefined,
        originalColor: new Cesium.Color()
    };
    // Information about the currently selected feature
    var selected = {
        feature: undefined,
        originalColor: new Cesium.Color()
    };

    // An entity object which will hold info about the currently selected feature for infobox display
    var selectedEntity = new Cesium.Entity();

    // Get default left click handler for when a feature is not picked on left click
    var clickHandler = viewer.screenSpaceEventHandler.getInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK);
    // Color a feature yellow on hover.
    viewer.screenSpaceEventHandler.setInputAction(function onMouseMove(movement) {
        if (Pickers_3DTile_Activated) {
            // If a feature was previously highlighted, undo the highlight
            if (Cesium.defined(highlighted.feature)) {
                highlighted.feature.color = highlighted.originalColor;
                highlighted.feature = undefined;
            }
            // Pick a new feature
            var picked3DtileFeature = viewer.scene.pick(movement.endPosition);
            if (!Cesium.defined(picked3DtileFeature)) {
                // nameOverlay.style.display = 'none';
                return;
            }
            // A feature was picked, so show it's overlay content
            // nameOverlay.style.display = 'block';
            // nameOverlay.style.bottom = viewer.canvas.clientHeight - movement.endPosition.y + 'px';
            // nameOverlay.style.left = movement.endPosition.x + 'px';
            var name = picked3DtileFeature.getProperty('name');
            if (!Cesium.defined(name)) {
                name = picked3DtileFeature.getProperty('OBJECTID');
            }
            // nameOverlay.textContent = name;
            // Highlight the feature if it's not already selected.
            if (picked3DtileFeature !== selected.feature) {
                highlighted.feature = picked3DtileFeature;
                Cesium.Color.clone(picked3DtileFeature.color, highlighted.originalColor);
                picked3DtileFeature.color = Cesium.Color.YELLOW;
            }
        }
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

    // Color a feature on selection and show metadata in the InfoBox.
    viewer.screenSpaceEventHandler.setInputAction(function onLeftClick(movement) {
        if (Pickers_3DTile_Activated) {
            // If a feature was previously selected, undo the highlight
            if (Cesium.defined(selected.feature)) {
                selected.feature.color = selected.originalColor;
                selected.feature = undefined;
            }
            // Pick a new feature
            var picked3DtileFeature = viewer.scene.pick(movement.position);
            last_picked_3DTiles = picked3DtileFeature;
            if (!Cesium.defined(picked3DtileFeature)) {
                clickHandler(movement);
                return;
            }
            // Select the feature if it's not already selected
            if (selected.feature === picked3DtileFeature) {
                return;
            }
            selected.feature = picked3DtileFeature;
            // Save the selected feature's original color
            if (picked3DtileFeature === highlighted.feature) {
                Cesium.Color.clone(highlighted.originalColor, selected.originalColor);
                highlighted.feature = undefined;
            } else {
                Cesium.Color.clone(picked3DtileFeature.color, selected.originalColor);
            }
            // Highlight newly selected feature
            picked3DtileFeature.color = Cesium.Color.LIME;
            // Set feature infobox description
            var featureName = picked3DtileFeature.getProperty('gml_name');
            selectedEntity.name = "Building Information";
            selectedEntity.description = 'Loading <div class="cesium-infoBox-loading"></div>';
            viewer.selectedEntity = selectedEntity;
            selectedEntity.description = '<table class="cesium-infoBox-defaultTable"><tbody>';
            if (picked3DtileFeature.getProperty('OBJECTID') !== undefined) {
                selectedEntity.description += '<tr><th>OBJECTID</th><td>' + picked3DtileFeature.getProperty('OBJECTID') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('gml_id') !== undefined) {
                selectedEntity.description += '<tr><th>gml-ID</th><td>' + picked3DtileFeature.getProperty('gml_id') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('gmlID') !== undefined) {
                selectedEntity.description += '<tr><th>gml-ID</th><td>' + picked3DtileFeature.getProperty('gmlID') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('file_candidate_gmlid') !== undefined) {
                selectedEntity.description += '<tr><th>file_candidate_gmlid</th><td>' + picked3DtileFeature.getProperty('file_candidate_gmlid') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('FeatureType') !== undefined) {
                selectedEntity.description += '<tr><th>FeatureType</th><td>' + picked3DtileFeature.getProperty('FeatureType') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('LocalityName') !== undefined) {
                selectedEntity.description += '<tr><th>LocalityName</th><td>' + picked3DtileFeature.getProperty('LocalityName') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('YearOfConstruction') !== undefined) {
                selectedEntity.description += '<tr><th>YearOfConstruction</th><td>' + picked3DtileFeature.getProperty('YearOfConstruction') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('FeatureType') !== undefined) {
                selectedEntity.description += '<tr><th>FeatureType</th><td>' + picked3DtileFeature.getProperty('FeatureType') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('RoofType') !== undefined) {
                selectedEntity.description += '<tr><th>RoofType</th><td>' + picked3DtileFeature.getProperty('RoofType') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('ThoroughfareName') !== undefined) {
                selectedEntity.description += '<tr><th>ThoroughfareName</th><td>' + picked3DtileFeature.getProperty('ThoroughfareName') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('Function') !== undefined) {
                selectedEntity.description += '<tr><th>Function</th><td>' + picked3DtileFeature.getProperty('Function') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('StoreysBelowGround') !== undefined) {
                selectedEntity.description += '<tr><th>StoreysBelowGround</th><td>' + picked3DtileFeature.getProperty('StoreysBelowGround') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('averageStoreyHeight') !== undefined) {
                selectedEntity.description += '<tr><th>averageStoreyHeight</th><td>' + picked3DtileFeature.getProperty('averageStoreyHeight') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('ThoroughfareNumber') !== undefined) {
                selectedEntity.description += '<tr><th>ThoroughfareNumber</th><td>' + picked3DtileFeature.getProperty('ThoroughfareNumber') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('StoreysAboveGround') !== undefined) {
                selectedEntity.description += '<tr><th>StoreysAboveGround</th><td>' + picked3DtileFeature.getProperty('StoreysAboveGround') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('Latitude') !== undefined) {
                selectedEntity.description += '<tr><th>Latitude</th><td>' + picked3DtileFeature.getProperty('Latitude') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('Longitude') !== undefined) {
                selectedEntity.description += '<tr><th>Longitude</th><td>' + picked3DtileFeature.getProperty('Longitude') + '</td></tr>'
            }


            if (picked3DtileFeature.getProperty('gml_parent_id') !== undefined) {
                selectedEntity.description += '<tr><th>gml_parent_id</th><td>' + picked3DtileFeature.getProperty('gml_parent_id') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('gml_name') !== undefined) {
                selectedEntity.description += '<tr><th>gml_name</th><td>' + picked3DtileFeature.getProperty('gml_name') + '</td></tr>'
            }
            if (picked3DtileFeature.getProperty('measuredHeight') !== undefined) {
                selectedEntity.description += '<tr><th>measuredHeight</th><td>' + picked3DtileFeature.getProperty('measuredHeight') + '</td></tr>'
            }
            selectedEntity.description += '</tbody></table>'

        }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
}

active3DTilePicker();