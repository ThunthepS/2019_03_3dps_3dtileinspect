Cesium.BingMapsApi.defaultKey = 'ApOW9LMkerqWIVSnFauilSeaZyp8df66byy1USCTjgTdMvhb4y1iAhEsUHQfCgzq'
Cesium.Ion.defaultAccessToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJmOGQ1ODcwNC0zNDU1LTQ5ZGEtOWE4YS0zYzNmYTcxOWQxYTUiLCJpZCI6MTAxMywiaWF0IjoxNTI2NjM1Nzg4fQ.X43-3OHqmcwPP5qCE2NqSGJUsj5FGWgXAzitvrV0pwY';

var viewer = new Cesium.Viewer('cesiumContainer', {
    terrainProvider: Cesium.createWorldTerrain(),
    baseLayerPicker: true,
    homeButton: true,
    scene3DOnly: true,
    //terrainExaggeration: 1.5,
    navigationHelpButton: false,
    selectionIndicator: false,
    sceneModePicker: false,
    geocoder: true,
    shadows: false,
});
// Basic Set Up for the Cesium Globe
viewer.scene.globe.enableLighting = true;
// viewer.extend(Cesium.viewerCesium3DTilesInspectorMixin);
// var inspectorViewModel = viewer.cesium3DTilesInspector.viewModel;

  
// //initial camera 
// var homeCameraView = {
//     destination: {
//         x: 4138789.3229200086,
//         y: 612789.6783915799,
//         z: 4798201.190526457
//     },
//     orientation: {
//         direction: {
//             x: -0.8915645712611195,
//             y: 0.44698338532571574,
//             z: 0.07292783086544413
//         },
//         up: {
//             x: 0.2518506964102406,
//             y: 0.35548990831326327,
//             z: 0.9001100776044587
//         }
//     }
// };
// // Add some camera flight animation options
// homeCameraView.duration = 2.0;
// homeCameraView.maximumHeight = 2000;
// homeCameraView.pitchAdjustHeight = 2000;
// homeCameraView.endTransform = Cesium.Matrix4.IDENTITY;
// viewer.scene.camera.setView(homeCameraView);
// viewer.homeButton.viewModel.command.beforeExecute.addEventListener(function (e) {
//     e.cancel = true;
//     viewer.scene.camera.flyTo(homeCameraView);
// });
viewer.clock.shouldAnimate = true; // default00
viewer.clock.startTime = Cesium.JulianDate.fromIso8601("2018-11-24T12:00:00Z");
viewer.clock.stopTime = Cesium.JulianDate.fromIso8601("2018-11-24T18:00:00Z");
viewer.clock.currentTime = Cesium.JulianDate.fromIso8601("2018-11-24T12:00:00Z");
viewer.clock.multiplier = 0.5; // sets a speedup
viewer.clock.clockRange = Cesium.ClockRange.LOOP_STOP; // loop at the end
viewer.timeline.zoomTo(viewer.clock.startTime, viewer.clock.stopTime); // set visible range

var tileset, tileset_door, tileset_roof, tileset_ground;
var tileset_wuestenrot;
var heightOffset = 49;

function call_3dps() {
    var input_bbox1 = $('#input_bbox1').val();
    var input_bbox2 = $('#input_bbox2').val();
    console.log('=== Calling 3DPS service ===')
    var request_url = 'http://193.196.37.89:8092/service/v1?service=3DPS&acceptversions=1.0&request=GetScene&boundingbox=' + input_bbox1 + ',' + input_bbox2 + '&separateparts=true';
    $.getJSON(request_url, function (data) {
        var result_url = data[0]["url"]
        console.log('=== Result URL : ' + request_url + ' ===')
        addLayers3DPS_SK(result_url) 
    });
}

//Tileset Stoeckach
var tileset_SK;
var addLayers3DPS_SK = function (request_url) {
    // CesiumStyleChoice.style.display = "block";
    // $("#CesiumStyleChoice").show("1000");
    try {
        tileset_SK = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: request_url,
            // url: "./Datas_3D/test-stoeckach-roof-noagg-overideParrent/tileset.json",
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    tileset_SK.readyPromise.then(function () {
        var boundingSphere = tileset_SK.boundingSphere;
        var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
        var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
        var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
        var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
        tileset_SK.modelMatrix = Cesium.Matrix4.fromTranslation(translation);

        viewer.zoomTo(tileset_SK);
    });
    // Styling the Defalut style
    // var transparentStyle = new Cesium.Cesium3DTileStyle({
    //     color: "color('WHITE', 1)",
    //     show: true
    // });
    // tileset_SK.style = transparentStyle;
};

//Tileset Stoeckach
var tileset_MC;
var addLayers_MC = function (request_url) {
    // CesiumStyleChoice.style.display = "block";
    // $("#CesiumStyleChoice").show("1000");
    try {
        tileset_MC = viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
            url: "./Datas_3D/3DTiles-Manchester-GeoT-Preston/tileset.json",
            // url: "./Datas_3D/test-stoeckach-roof-noagg-overideParrent/tileset.json",
            maximumScreenSpaceError: 8 // default value
        }));

    } catch (err) {
        console.log('->  add 3DTiles failed!\n' + err);
    }
    tileset_MC.readyPromise.then(function () {
        var boundingSphere = tileset_MC.boundingSphere;
        var cartographic = Cesium.Cartographic.fromCartesian(boundingSphere.center);
        var surface = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, 0.0);
        var offset = Cesium.Cartesian3.fromRadians(cartographic.longitude, cartographic.latitude, heightOffset);
        var translation = Cesium.Cartesian3.subtract(offset, surface, new Cesium.Cartesian3());
        tileset_MC.modelMatrix = Cesium.Matrix4.fromTranslation(translation);

        viewer.zoomTo(tileset_MC);
    });
    // Styling the Defalut style
    // var transparentStyle = new Cesium.Cesium3DTileStyle({
    //     color: "color('WHITE', 1)",
    //     show: true
    // });
    // tileset_MC.style = transparentStyle;
};
//addLayers_MC();

// addLayers3DT();
// addLayers3DT_Sample();
// addLayers3DT_Separate();


var zoomToTileset = function () {
    viewer.zoomTo(tileset);
}

var zoomToWR = function () {
    viewer.zoomTo(tileset_wuestenrot);
}

var zoomToND = function () {
    viewer.zoomTo(tileset_ND);
}